import django.http
from django.contrib.admin import AdminSite
from django.test import TestCase

import django_adminstats.admin
import django_adminstats.models
import django_adminstats.registry

# needed to register the admin
from django.contrib import admin


class MockAnon:
    is_authenticated = False
    is_active = False
    is_staff = False

    @staticmethod
    def has_perm(_perm):
        return False


class MockSuperUser:
    is_authenticated = True
    is_active = True
    is_staff = True

    @staticmethod
    def has_perm(_perm):
        return True

    @staticmethod
    def has_module_perms(_app_label):
        return True


class MockRequest:

    def __init__(self, method, parameters=None):
        self.method = method
        self.user = MockSuperUser()
        self.GET = django.http.QueryDict(mutable=True)
        self.POST = django.http.QueryDict(mutable=True)
        if parameters is not None:
            if method == 'POST':
                self.POST.update(parameters)
            else:
                self.GET.update(parameters)
        self.META = {'SCRIPT_NAME': ''}

    def get_full_path(self):
        return '/mock/path'

    def build_absolute_uri(self):
        return 'http://127.0.0.1/mock/path'


class ChartTests(TestCase):
    """Test chart CRUD pages"""

    fixtures = ['demo']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model_class = django_adminstats.models.Chart
        self.admin = django_adminstats.admin.ChartAdmin(
            self.model_class, AdminSite())

    def test_copy(self):
        django_adminstats.admin.copy_chart(
            None, None, self.model_class.objects.filter(pk=2))
        new = self.model_class.objects.order_by('-id')[0]
        assert new.title == 'Copy of Daily Page Views'
        assert new.criteria.count() == 1

    def test_chart(self):
        obj = self.model_class.objects.get(pk=2)
        request = MockRequest('GET', {})
        response = self.admin.view_chart(request, str(obj.id))
        assert response.status_code == 200
        assert '<td>3</td>' in response.rendered_content

    def test_chart404(self):
        request = MockRequest('GET', {})
        response = self.admin.view_chart(request, '404')
        assert response.status_code == 404

    def test_chart405(self):
        request = MockRequest('POST', {})
        response = self.admin.view_chart(request, '2')
        assert response.status_code == 405

    def test_chart403(self):
        request = MockRequest('GET', {})
        request.user = MockAnon()
        response = self.admin.view_chart(request, '2')
        assert response.status_code == 403


class ExploreTests(TestCase):
    """Test explore pages"""

    fixtures = ['demo']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        model_class = django_adminstats.models.Chart
        self.admin = django_adminstats.admin.ChartAdmin(
            model_class, admin.site)

    def test_chart_pageview_by_date(self):
        request = MockRequest('GET', {'_x': 'date'})
        response = self.admin.explore(request, 'tests', 'pageview')
        assert response.status_code == 200
        # table top
        assert '<th>November</th>' in response.rendered_content
        # date headers
        assert '>6</a></th>' in response.rendered_content
        assert '>7</a></th>' in response.rendered_content
        assert '>8</a></th>' in response.rendered_content
        # values
        assert '>2</a></td>' in response.rendered_content
        assert '>3</a></td>' in response.rendered_content

    def test_chart404(self):
        request = MockRequest('GET', {})
        response = self.admin.explore(request, 'tests', '404')
        assert response.status_code == 404

    def test_chart405(self):
        request = MockRequest('POST', {})
        response = self.admin.explore(request, 'tests', 'pageview')
        assert response.status_code == 405

    def test_chart_anon(self):
        """
        Anonymous users get redirected to login
        """
        request = MockRequest('GET', {})
        request.user = MockAnon()
        response = self.admin.explore(request, 'tests', 'pageview')
        assert response.status_code == 302


def test_autocomplete_view_perm():
    stats_key = list(django_adminstats.registry.REGISTRY.keys())[0]

    su_request = MockRequest('GET', {})
    anon_request = MockRequest('GET', {})
    anon_request.user = MockAnon()

    view_func = django_adminstats.admin.ChartAdmin.query_autocomplete_view
    view = django_adminstats.admin.QueryAutocompleteJsonView()
    # anon user should get 403
    assert not view.has_perm(anon_request)
    assert view_func(anon_request, stats_key).status_code == 403
    # admin user should not
    assert view.has_perm(su_request)
    assert view_func(su_request, stats_key).status_code == 200


def test_autocomplete_format_value():
    widget = django_adminstats.admin.QueryAutocomplete()
    assert ['foo', 'bar=baz'] == widget.format_value('foo&bar=baz')
    assert ['foo'] == widget.format_value('foo&')
    assert ['foo'] == widget.format_value('foo')
    assert [] == widget.format_value('')
    assert [] == widget.format_value(None)


def test_autocomplete_value_from_datadict():
    widget = django_adminstats.admin.QueryAutocomplete()
    assert 'foo&bar=baz' == widget.value_from_datadict(
        django.http.QueryDict('k=foo%26bar%3Dbaz'), {}, 'k')
    assert 'foo&' == widget.value_from_datadict(
        django.http.QueryDict('k=foo%26'), {}, 'k')
    assert 'foo' == widget.value_from_datadict(
        django.http.QueryDict('k=foo'), {}, 'k')
    assert '' == widget.value_from_datadict(
        django.http.QueryDict(''), {}, 'k')


def test_autocomplete_value_urls():
    assert ('/admin/django_adminstats/chart/query_autocomplete/'
            == django_adminstats.admin.QueryAutocomplete().get_url())
    assert ('/admin/django_adminstats/chart/filter_autocomplete/'
            == django_adminstats.admin.FilterAutocomplete().get_url())
