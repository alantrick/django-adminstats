from datetime import date

from django.test import TestCase

from django_adminstats import Step, registry

from . import models


class QueryModelTests(TestCase):
    """Test querying model data"""

    fixtures = ['demo']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        stats_key = getattr(models.PageView, '_meta').label_lower
        self.reg = registry.REGISTRY[stats_key]

    def test_blank_years(self):
        assert {} == self.reg.get_data(
            start=date(1900, 1, 1), end=date(1910, 1, 1),
            period_step=Step.YEAR, axis_text='', group_text='', filter_text='')

    def test_vanilla(self):
        expected = {'': {
            date(2018, 11, 6): '2',
            date(2018, 11, 7): '2',
            date(2018, 11, 8): '3',
        }}
        actual = self.reg.get_data(
            start=date(2018, 11, 1), end=date(2018, 11, 12),
            period_step=Step.DAY, axis_text='', group_text='', filter_text='')
        assert expected == actual

    def test_group_by_path(self):
        expected = {'/': {
            date(2018, 11, 6): '1',
            date(2018, 11, 7): '2',
            date(2018, 11, 8): '3',
        }, '/george': {
            date(2018, 11, 6): '1',
        }}
        actual = self.reg.get_data(
            start=date(2018, 11, 1), end=date(2018, 11, 12),
            period_step=Step.DAY, axis_text='', group_text='page__path',
            filter_text='')
        assert expected == actual


class QueryTrackstatsTests(TestCase):
    """Test querying trackstats data"""

    fixtures = ['demo']

    @staticmethod
    def test_blank_years():
        reg = registry.REGISTRY['trackstats:page.view']
        assert {} == reg.get_data(
            start=date(1900, 1, 1), end=date(1910, 1, 1),
            period_step=Step.YEAR, axis_text='', group_text='', filter_text='')

    @staticmethod
    def test_vanilla():
        reg = registry.REGISTRY['trackstats:page.view']
        expected = {'': {
            date(2018, 11, 6): '2',
            date(2018, 11, 7): '2',
            date(2018, 11, 8): '3',
        }}
        actual = reg.get_data(
            start=date(2018, 11, 1), end=date(2018, 11, 12),
            period_step=Step.DAY, axis_text='', group_text='', filter_text='')
        assert expected == actual

    @staticmethod
    def test_bad_axis():
        reg = registry.REGISTRY['trackstats:page.view']
        try:
            reg.get_data(
                start=date(2018, 11, 1), end=date(2018, 11, 12),
                period_step=Step.DAY, axis_text='period', group_text='',
                filter_text='')
        except ValueError:
            pass
        else:
            assert False, 'Function should have raised ValueError because ' \
                          'axis query isn\'t supported in tracstats'

    @staticmethod
    def test_by_month():
        reg = registry.REGISTRY['trackstats:page.view']
        # note: we're only going to get results for november, because
        # we don't have stats in the fixtures for any other month
        expected = {'': {
            date(2018, 11, 1): '7',
        }}
        actual = reg.get_data(
            start=date(2018, 10, 1), end=date(2018, 12, 1),
            period_step=Step.MONTH, axis_text='',
            group_text='', filter_text='')
        assert expected == actual

    # Note: this is not the desired behavior, but it is the current behavior
    @staticmethod
    def test_group_by_path():
        reg = registry.REGISTRY['trackstats:page.view:tests.page']
        expected = {'/': {
            date(2018, 11, 6): '1',
            date(2018, 11, 7): '2',
            date(2018, 11, 8): '3',
        }, '/george': {
            date(2018, 11, 6): '1',
        }}
        actual = reg.get_data(
            start=date(2018, 11, 1), end=date(2018, 11, 12),
            period_step=Step.DAY, axis_text='',
            group_text='object__path', filter_text='')
        assert expected == actual

    @staticmethod
    def test_multi_group_by():
        reg = registry.REGISTRY['trackstats:page.view:tests.page']
        expected = {'/ / 1': {
            date(2018, 11, 6): '1',
            date(2018, 11, 7): '2',
            date(2018, 11, 8): '3',
        }, '/george / 2': {
            date(2018, 11, 6): '1',
        }}
        actual = reg.get_data(
            start=date(2018, 11, 1), end=date(2018, 11, 12),
            period_step=Step.DAY, axis_text='',
            group_text='object__path&object__id', filter_text='')
        assert expected == actual
