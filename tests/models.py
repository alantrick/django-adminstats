from django.utils.translation import gettext_lazy as _
from django.db import models
from trackstats.models import (Domain, Metric, Period, StatisticByDate,
                               StatisticByDateAndObject)

from django_adminstats.registry import register_metric, register_model

Domain.objects.PAGES = Domain.objects.register(
    ref='page', name='Page Stats')

Metric.objects.PAGE_VIEWS = Metric.objects.register(
    ref='view', domain=Domain.objects.PAGES, name='Page views')


class Category(models.Model):

    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Page(models.Model):

    path = models.CharField(max_length=1000)
    category = models.ManyToManyField(Category, blank=True)

    def __str__(self):
        return self.path


class PageView(models.Model):

    page = models.ForeignKey(
        Page, on_delete=models.CASCADE, related_name='views')
    ip_address = models.GenericIPAddressField()
    date = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # auto-generate trackstats stats
        today = self.date.date()
        count = PageView.objects.filter(
            date__year=today.year,
            date__month=today.month,
            date__day=today.day,
        ).count()
        StatisticByDate.objects.record(
            metric=Metric.objects.PAGE_VIEWS,
            value=count,
            period=Period.DAY,
            date=today
        )
        count = PageView.objects.filter(
            date__year=today.year,
            date__month=today.month,
            date__day=today.day,
            page=self.page,
        ).count()
        StatisticByDateAndObject.objects.record(
            metric=Metric.objects.PAGE_VIEWS,
            value=count,
            period=Period.DAY,
            object=self.page,
            date=today
        )


class CustomerAddress(models.Model):
    lines = models.TextField(_("Address Lines"))
    city = models.CharField(_("City"), max_length=200)
    post_code = models.CharField(_("Postal Code"), max_length=50)

    def __str__(self):
        return '{}, {}, {}'.format(self.lines, self.city, self.post_code)


class CustomerGroup(models.Model):
    name = models.CharField(_("Name"), max_length=200)

    def __str__(self):
        return self.name


class Customer(models.Model):

    HEAR_OF_METHOD = (
        ('web ads', _("Online Advertisements")),
        ('djpack', _("djangopackages.org")),
        ('pypi', _("PyPI")),
        ('friend', _("A friend")),
        ('other', _("Other")),
    )

    name = models.CharField(_("Name"), max_length=200)
    email = models.EmailField(_("Email"), max_length=200)
    birthday = models.DateField(_("Birthday"), null=True, blank=True)
    groups = models.ManyToManyField(
        verbose_name=_("Group"), to=CustomerGroup, related_name='customers')

    billing_address = models.OneToOneField(
        to=CustomerAddress,
        null=True, blank=True, on_delete=models.SET_NULL)

    hear_of_method = models.CharField(
        _("How did you hear about us?"),
        choices=HEAR_OF_METHOD, max_length=100)
    is_verified = models.BooleanField(
        _("Verified"), default=False, help_text=_(
            "Check if you know this actually a real customer"))

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Inventory(models.Model):

    name = models.CharField(_("Name"), max_length=200)
    stock_quantity = models.PositiveIntegerField(
        _("Quantity in stock"), default=0)
    price = models.DecimalField(_("Price"), max_digits=20, decimal_places=2)


class Sale(models.Model):

    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)
    Inventory = models.ForeignKey(Inventory, on_delete=models.PROTECT)
    quantity = models.PositiveIntegerField(_("Quantity"))
    price_paid = models.DecimalField(
        _("Price paid"), max_digits=20, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True)


register_metric(Metric.objects.PAGE_VIEWS)
register_metric(Metric.objects.PAGE_VIEWS, Page)
register_model(PageView)
