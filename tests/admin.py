from django.contrib import admin

from . import models


class PageViewAdmin(admin.ModelAdmin):

    list_display = ['ip_address', 'date']
    list_filter = ['ip_address', 'page', 'page__category', 'date']
    date_hierarchy = 'date'


class CustomerAdmin(admin.ModelAdmin):

    list_display = ['name', 'birthday']
    list_filter = ['groups', 'billing_address__city',
                   'hear_of_method', 'is_verified']
    filter_horizontal = ['groups']


admin.site.register(models.Page)
admin.site.register(models.PageView, PageViewAdmin)
admin.site.register(models.Customer, CustomerAdmin)
