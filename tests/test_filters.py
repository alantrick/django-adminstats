from django_adminstats import qs, registry

from . import models


def test_pageview_base_query_options():
    qsp = qs.QuerySpecPart('', is_filter=False)
    expected = ['id', 'page', 'ip_address', 'date']
    assert expected == qsp.options(models.PageView)


def test_pageview_page_query_options():
    qsp = qs.QuerySpecPart('page', is_filter=False)
    expected = ['page', 'page__id', 'page__path', 'page__category']
    assert expected == qsp.options(models.PageView)


def test_pageview_page_path_query_options():
    qsp = qs.QuerySpecPart('page__path', is_filter=False)
    expected = ['page__path', 'page__path:count']
    assert expected == qsp.options(models.PageView)


def test_pageview_page_category_query_options():
    qsp = qs.QuerySpecPart('page__category', is_filter=False)
    expected = ['page__category', 'page__category__id', 'page__category__name']
    assert expected == qsp.options(models.PageView)


def test_pageview_partial_query_options():
    qsp = qs.QuerySpecPart('ip', is_filter=False)
    assert ['ip_address'] == qsp.options(models.PageView)


def test_pageview_field_query_options():
    qsp = qs.QuerySpecPart('ip_address', is_filter=False)
    expected = ['ip_address', 'ip_address:count']
    assert expected == qsp.options(models.PageView)


def test_pageview_funky_field_query_options():
    # this is somewhat strange behavior, but it's how the django querying works
    qsp = qs.QuerySpecPart('page_id', is_filter=False)
    expected = ['page_id', 'page_id__id', 'page_id__path', 'page_id__category']
    assert expected == qsp.options(models.PageView)


def test_pageview_partial_lookup_query_options():
    qsp = qs.QuerySpecPart('ip_address__i', is_filter=False)
    expected = ['ip_address']
    assert expected == qsp.options(models.PageView)


def test_pageview_lookup_query_options():
    qsp = qs.QuerySpecPart('ip_address__iexact', is_filter=False)
    expected = ['ip_address']
    assert expected == qsp.options(models.PageView)


# I don't think some of these functions make sense, but they
# are technically allowed
def test_pageview_func_empty_query_options():
    qsp = qs.QuerySpecPart('ip_address:', is_filter=False)
    expected = ['ip_address', 'ip_address:count', 'ip_address:sum',
                'ip_address:avg', 'ip_address:min', 'ip_address:max',
                'ip_address:stddev', 'ip_address:variance']
    assert expected == qsp.options(models.PageView)


def test_pageview_func_partial_query_options():
    qsp = qs.QuerySpecPart('ip_address:m', is_filter=False)
    expected = ['ip_address', 'ip_address:min', 'ip_address:max']
    assert expected == qsp.options(models.PageView)


def test_pageview_base_filter_options():
    qsp = qs.QuerySpecPart('', is_filter=True)
    expected = ['id=', 'page=', 'ip_address=', 'date=']
    assert expected == qsp.options(models.PageView)


def test_pageview_equals_filter_options():
    qsp = qs.QuerySpecPart('id=', is_filter=True)
    expected = ['id=', 'id:count=', 'id__exact=', 'id__iexact=', 'id__gt=',
                'id__gte=', 'id__lt=', 'id__lte=', 'id__in=', 'id__contains=',
                'id__icontains=', 'id__startswith=', 'id__istartswith=',
                'id__endswith=', 'id__iendswith=', 'id__range=', 'id__isnull=',
                'id__regex=', 'id__iregex=']
    assert expected == qsp.options(models.PageView)


def test_pageview_equals_value_filter_options():
    qsp = qs.QuerySpecPart('id=12', is_filter=True)
    assert ['id=12'] == qsp.options(models.PageView)


def test_pageview_field_filter_options():
    qsp = qs.QuerySpecPart('ip_address', is_filter=True)
    expected = ['ip_address=', 'ip_address:count=',
                'ip_address__exact=', 'ip_address__iexact=',
                'ip_address__gt=', 'ip_address__gte=', 'ip_address__lt=',
                'ip_address__lte=', 'ip_address__in=', 'ip_address__contains=',
                'ip_address__icontains=', 'ip_address__startswith=',
                'ip_address__istartswith=', 'ip_address__endswith=',
                'ip_address__iendswith=', 'ip_address__range=',
                'ip_address__isnull=', 'ip_address__regex=',
                'ip_address__iregex=']
    assert expected == qsp.options(models.PageView)


def test_pageview_partial_lookup_filter_options():
    qsp = qs.QuerySpecPart('ip_address__i', is_filter=True)
    expected = ['ip_address=', 'ip_address__iexact=', 'ip_address__in=',
                'ip_address__icontains=', 'ip_address__istartswith=',
                'ip_address__iendswith=', 'ip_address__isnull=',
                'ip_address__iregex=']
    assert expected == qsp.options(models.PageView)


def test_pageview_lookup_filter_options():
    qsp = qs.QuerySpecPart('ip_address__iexact', is_filter=True)
    expected = ['ip_address__iexact=', 'ip_address__iexact:count=']
    assert expected == qsp.options(models.PageView)


def test_modelregistry_query_options():
    reg = registry.REGISTRY['tests.pageview']
    expected = ['id', 'page', 'ip_address', 'date']
    assert expected == reg.query_options('', is_filter=False)


def test_trackstats_nomodel_options():
    reg = registry.REGISTRY['trackstats:page.view']
    assert [] == reg.query_options('', is_filter=False)


def test_trackstats_base_options():
    reg = registry.REGISTRY['trackstats:page.view:tests.page']
    expected = ['id', 'metric', 'value', 'period', 'object_type',
                'object_id', 'date', 'object']
    assert expected == reg.query_options('', is_filter=False)


def test_trackstats_object_options():
    reg = registry.REGISTRY['trackstats:page.view:tests.page']
    expected = ['object__id', 'object__path', 'object__category']
    assert expected == reg.query_options('object', is_filter=False)


def test_trackstats_object_under_options():
    reg = registry.REGISTRY['trackstats:page.view:tests.page']
    expected = ['object__id', 'object__path', 'object__category']
    assert expected == reg.query_options('object_', is_filter=False)


def test_trackstats_object_partial_options():
    reg = registry.REGISTRY['trackstats:page.view:tests.page']
    expected = ['object__path']
    assert expected == reg.query_options('object__p', is_filter=False)


def test_trackstats_object_path_options():
    reg = registry.REGISTRY['trackstats:page.view:tests.page']
    expected = ['object__path', 'object__path:count']
    assert expected == reg.query_options('object__path', is_filter=False)
