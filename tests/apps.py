import django.apps
from django.core.management import call_command

from django.test.utils import setup_databases


class Config(django.apps.AppConfig):
    name = 'tests'
    verbose_name = 'Testing'


class TestConfig(Config):

    def ready(self):
        setup_databases(verbosity=3, interactive=False)


class DemoConfig(Config):

    def ready(self):
        setup_databases(verbosity=3, interactive=False)
        # add notification objects
        call_command('loaddata', 'demo')
