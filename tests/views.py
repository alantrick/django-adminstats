from django.shortcuts import render

from . import models


def index(request):
    page, _ = models.Page.objects.get_or_create(path=request.path)
    models.PageView.objects.create(
        page=page, ip_address=request.META['REMOTE_ADDR'])
    context = {'count': models.PageView.objects.count()}
    return render(request, 'tests/index.txt', context)
