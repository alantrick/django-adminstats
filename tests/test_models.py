import unittest.mock
from datetime import date, datetime

from django_adminstats import Step, models


@unittest.mock.patch('django.utils.timezone.now')
def test_chart_get_end_date(mock_now):
    mock_now.return_value = datetime(2005, 2, 10)

    expected_results = (
        # note that the end date is the date after
        # the last one
        ('t', Step.DAY.value, date(2005, 2, 11)),
        ('y', Step.DAY.value, date(2005, 2, 10)),
        ('t', Step.MONTH.value, date(2005, 3, 1)),
        ('y', Step.MONTH.value, date(2005, 2, 1)),
        ('t', Step.YEAR.value, date(2006, 1, 1)),
        ('y', Step.YEAR.value, date(2005, 1, 1)),
    )

    for until_type, step_type, end_date in expected_results:
        chart = models.Chart(until_type=until_type, period_step=step_type)
        assert end_date == chart.get_end_date()

    # special check for month overflow
    mock_now.return_value = datetime(2005, 12, 10)
    chart = models.Chart(until_type='t', period_step=Step.MONTH.value)
    assert date(2006, 1, 1) == chart.get_end_date()


def test_chart_move_delta():
    now = date(2005, 2, 10)

    expected_results = (
        # note that the end date is the date after
        # the last one
        (0, Step.DAY.value, date(2005, 2, 10)),
        (30, Step.DAY.value, date(2005, 1, 11)),
        (0, Step.MONTH.value, date(2005, 2, 10)),
        (30, Step.MONTH.value, date(2002, 8, 10)),
        (0, Step.YEAR.value, date(2005, 2, 10)),
        (1, Step.YEAR.value, date(2004, 2, 10)),
    )

    for num, step_type, start_date in expected_results:
        chart = models.Chart(period_step=step_type)
        assert start_date == now - chart.move_delta(num)


def test_criteria_stats_key():
    criteria = models.Criteria(stats_key='foo')
    assert 'foo' == str(criteria)
