CHANGES
=======

0.7.4
-----

* Use the view instead of change permission for viewing charts

0.7.3
-----

* Fix error in description generation

0.7.2
-----
* Make chart control list items more visually distinct

0.7.1
-----

* Fix packaging problem (missing files)

0.7.0
-----

* Add new “Explore” charts, upgrading users may want to adjust
  ``INSTALLED_APPS``.

0.6.3
-----

* Drop pbr for regular setuptools, add VERSION attribute
* Fix some new problems that flake was catching
* Add screenshot of chart

0.6.2
-----

* Fix problem where criteria with same stats would clobber each other

0.6.1
-----

* fix programming bug with by-month trackstats metrics, better test coverage

0.6
---

* fix autocomplete options for tracstats criteria
* Fix problems with the autocomplete not (re)initializing properly

0.5
---

* Add autocomplete for query fields in admin, and rename query fields

0.4
---

* Add documentation for query fields and registrations
* Add copy chart admin action

0.3
---

* Add the ability to filter/group related models in trackstats by-object
  stats registrations
